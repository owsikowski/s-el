(hissp.basic.._macro_.prelude)

;python was a nice language when it's main working horse was the list
;now map returns a "map object".
; and filter a filter object
; it does not respect the programmers time!!!
; i thought dash.el will be the next project to port,
; but i have enough of my time wasted by this nonsense
; don't do OOP. Ever. It cripples your cognitive skills and esthetic perception.
;If you want to make good software then please unlearn this nonsense.
;OOP contains some functions that do the work and 90% of bugridden and ugly codebloat that does not have any use whatsoever
;there were numerous studies proving that ppl programming functional are MUCH more productive
;besides the code is more concise and breve. It's really hard to do buggy software in this style. Either code runs or it does not
;If you think you are smart then dump OOP asap. OOP was created for the dumb masses, not the intelligent individuals.
;the sooner you unlearn this nonsense the better for the world
;please, please, please, dump this nonsense for the sake of humanity
;you'll make way more money and have more time to read books
; Most IT professionals have never heard of Caravaggio.
; It's a shame!!! If they wouldn't waste their time on OOP nonsense they would.
; Compressed emacs(which actually is a 40year old hack and not latest state of the art tech) is smth like 40mb and has much more functionality then 2gb intellij. Or take a look how bloated, slow and disgusting windows is compared to linux. Or what a pile of crap android is. Put some OOP into the mix and this is ALWAYS the final result.
; it's so simple: one action - one function,
; you can describe everything in a simple way if you do it functional.
; you don't need to wrap it up with useless codebloat and ceremony
; IT was created by mathematicians. There are only functions. Functions are a special case of sets. You don't need anything else.
; OOP is lobotomy. Don't do this.
; Or if you don't have enough then take a look at python regexps that return "match objects". Fantastic. Now you have to waste your time and read how this nonsense works. Instead of doing something creative you waste your time by reading really boring docs and you will produce bugridden software.
;The real work is done always by functions. Everything else is obfuscation of the real design and algorithm. It's bizarre and occult. It stinks.
; Just say no.




; these babies belong as already mentioned to the upcoming dash.el port
; dash.el is awesome! I'm sure it's somehow related to kal-el, that's how awesome it is!!!
(define -map (lambda (f l) (list (map f l))))
(define -filter (lambda (f l) (list (filter f l))))
; and some other stuff i think belong to a proper prelude
(define + (lambda (: :* rest) (reduce operator..add rest)))
(define first (lambda (l) .#"l[0]"))

;------------------------------
;---------------- here we go!!! s.el go, go, go! The best string lib EVER <3

(define s-trim (lambda (s) (.strip s) ))
;
;Remove whitespace at the beginning and end of s.
;
;(s-trim "trim ") ;; => "trim"
;(s-trim " this") ;; => "this"
;(s-trim "   trims beg and end  ") ;; => "only  trims beg and end"
;
(define s-trim-left (lambda (s) (.lstrip s) ))
;
;Remove whitespace at the beginning of s.
;
;(s-trim-left "trim ") ;; => "trim "
;(s-trim-left " this") ;; => "this"
;
(define s-trim-right (lambda (s) (.rstrip s) ))
;
;Remove whitespace at the end of s.
;
;(s-trim-right "trim ") ;; => "trim"
;(s-trim-right " this") ;; => " this"
;
(define s-chomp  (lambda (s) (if-else (.endswith s "\r\n") (.removesuffix s "\r\n") (.removesuffix s "\n"))))
;
;Remove one trailing \n, \r or \r\n from s.
;
;(s-chomp "no newlines\n") ;; => "no newlines"
;(s-chomp "no newlines\r\n") ;; => "no newlines"
;(s-chomp "some newlines\n\n") ;; => "some newlines\n"
;
(define s-collapse-whitespace  (lambda (s) (s-join " " (.split s ))))
;
;Convert all adjacent whitespace characters to a single space.
;
;(s-collapse-whitespace "only   one space   please") ;; => "only one space please"
;(s-collapse-whitespace "collapse \n all \t sorts of \r whitespace") ;; => "collapse all sorts of whitespace"
;
(define s-word-wrap  (lambda (len s) (s-join #"\n" (textwrap..wrap s len : break_long_words False break_on_hyphens False)  )))
;
;If s is longer than len, wrap the words with newlines.
;
;(s-word-wrap 10 "This is too long") ;; => "This is\ntoo long"
;(s-word-wrap 10 "This is way way too long") ;; => "This is\nway way\ntoo long"
;(s-word-wrap 10 "It-wraps-words-but-does-not-break-them") ;; => "It-wraps-words-but-does-not-break-them"
;
(define s-center  (lambda (len s) (.center s len)))
;
;If s is shorter than len, pad it with spaces so it is centered.
;
;(s-center 5 "a") ;; => "  a  "
;(s-center 5 "ab") ;; => "  ab "
;(s-center 1 "abc") ;; => "abc"
;
(define s-pad-left (lambda (len padding s) (.rjust s len padding)))
;
;If s is shorter than len, pad it with padding on the left.
;
;(s-pad-left 3 "0" "3") ;; => "003"
;(s-pad-left 3 "0" "23") ;; => "023"
;(s-pad-left 3 "0" "1234") ;; => "1234"
;
(define s-pad-right (lambda (len padding s) (.ljust s len padding)))
;
;If s is shorter than len, pad it with padding on the right.
;
;(s-pad-right 3 "." "3") ;; => "3.."
;(s-pad-right 3 "." "23") ;; => "23."
;(s-pad-right 3 "." "1234") ;; => "1234"
;
(define s-truncate (lambda (lennn s) (if-else .#"len(s)>lennn" .#"s[:lennn-3]+'...'" s)))
;
;If s is longer than len, cut it down to len - 3 and add ... at the end.
;
;(s-truncate 6 "This is too long") ;; => "Thi..."
;(s-truncate 16 "This is also too long") ;; => "This is also ..."
;(s-truncate 16 "But this is not!") ;; => "But this is not!"
;
(define s-left (lambda (len s) .#"s[:len]"))
;
;Returns up to the len first chars of s.
;
;(s-left 3 "lib/file.js") ;; => "lib"
;(s-left 3 "li") ;; => "li"
;
(define s-right (lambda (len s)  .#"s[-len:]"))
;
;Returns up to the len last chars of s.
;
;(s-right 3 "lib/file.js") ;; => ".js"
;(s-right 3 "li") ;; => "li"
;
(define s-chop-suffix (lambda (suffix s) (.removesuffix suffix s)))
;
;Remove suffix if it is at end of s.
;
;(s-chop-suffix "-test.js" "penguin-test.js") ;; => "penguin"
;(s-chop-suffix "\n" "no newlines\n") ;; => "no newlines"
;(s-chop-suffix "\n" "some newlines\n\n") ;; => "some newlines\n"
;
(define s-chop-suffixes (lambda (suffixes s) ))
;
;Remove suffixes one by one in order, if they are at the end of s.
;
;(s-chop-suffixes '("_test.js" "-test.js" "Test.js") "penguin-test.js") ;; => "penguin"
;(s-chop-suffixes '("\r" "\n") "penguin\r\n") ;; => "penguin\r"
;(s-chop-suffixes '("\n" "\r") "penguin\r\n") ;; => "penguin"
;
(define s-chop-prefix (lambda (prefix s) (.removeprefix prefix s)))
;
;Remove prefix if it is at the start of s.
;
;(s-chop-prefix "/tmp" "/tmp/file.js") ;; => "/file.js"
;(s-chop-prefix "/tmp" "/tmp/tmp/file.js") ;; => "/tmp/file.js"
;
(define s-chop-prefixes (lambda (prefixes s) ))
;
;Remove prefixes one by one in order, if they are at the start of s.
;
;(s-chop-prefixes '("/tmp" "/my") "/tmp/my/file.js") ;; => "/file.js"
;(s-chop-prefixes '("/my" "/tmp") "/tmp/my/file.js") ;; => "/my/file.js"
;
(define s-shared-start (lambda (s1 s2) ))
;
;Returns the longest prefix s1 and s2 have in common.
;
;(s-shared-start "bar" "baz") ;; => "ba"
;(s-shared-start "foobar" "foo") ;; => "foo"
;(s-shared-start "bar" "foo") ;; => ""
;
(define s-shared-end (lambda (s1 s2) ))
;
;Returns the longest suffix s1 and s2 have in common.
;
;(s-shared-end "bar" "var") ;; => "ar"
;(s-shared-end "foo" "foo") ;; => "foo"
;(s-shared-end "bar" "foo") ;; => ""
;
(define s-repeat operator..mul)
;
;Make a string of s repeated num times.
;
;(s-repeat 10 " ") ;; => "          "
;(s-concat (s-repeat 8 "Na") " Batman!") ;; => "NaNaNaNaNaNaNaNa Batman!"
;
(define s-concat (lambda (: :* strings) (functools..reduce operator..add strings)))
;
;Join all the string arguments into one string.
;
;(s-concat "abc" "def" "ghi") ;; => "abcdefghi"
;
(define s-prepend operator..add)
;
;Concatenate prefix and s.
;
;(s-prepend "abc" "def") ;; => "abcdef"
;
(define s-append (lambda (suffix s) (operator..add s suffix) ))
;
;Concatenate s and suffix.
;
;(s-append "abc" "def") ;; => "defabc"
;
(define s-lines (lambda (s) (.splitlines s) ))
;
;Splits s into a list of strings on newline characters.
;
;(s-lines "abc\ndef\nghi") ;; => '("abc" "def" "ghi")
;(s-lines "abc\rdef\rghi") ;; => '("abc" "def" "ghi")
;(s-lines "abc\r\ndef\r\nghi") ;; => '("abc" "def" "ghi")
;
(define s-match (lambda (regexp s &optional start) ))
;
;When the given expression matches the string, this function returns a list of the whole matching string and a string for each matched subexpressions. If it did not match the returned value is an empty list (nil).
;
;When start is non-nil the search will start at that index.
;
;(s-match "^def" "abcdefg") ;; => nil
;(s-match "^abc" "abcdefg") ;; => '("abc")
;(s-match "^/.*/\\([a-z]+\\)\\.\\([a-z]+\\)" "/some/weird/file.html") ;; => '("/some/weird/file.html" "file" "html")
;
(define s-match-strings-all (lambda (regex string) ))
;
;Return a list of matches for regex in string.
;
;Each element itself is a list of matches, as per match-string. Multiple matches at the same position will be ignored after the first.
;
;(s-match-strings-all "{\\([^}]+\\)}" "x is {x} and y is {y}") ;; => '(("{x}" "x") ("{y}" "y"))
;(s-match-strings-all "ab." "abXabY") ;; => '(("abX") ("abY"))
;(s-match-strings-all "\\<" "foo bar baz") ;; => '(("") ("") (""))
;
(define s-matched-positions-all (lambda (regexp string &optional subexp-depth) ))
;
;Return a list of matched positions for regexp in string. subexp-depth is 0 by default.
;
;(s-matched-positions-all "l+" "{{Hello}} World, {{Emacs}}!" 0) ;; => '((4 . 6) (13 . 14))
;(s-matched-positions-all "{{\\(.+?\\)}}" "{{Hello}} World, {{Emacs}}!" 0) ;; => '((0 . 9) (17 . 26))
;(s-matched-positions-all "{{\\(.+?\\)}}" "{{Hello}} World, {{Emacs}}!" 1) ;; => '((2 . 7) (19 . 24))
;
(define s-slice-at (lambda (regexp s) ))
;
;Slices s up at every index matching regexp.
;
;(s-slice-at "-" "abc") ;; => '("abc")
;(s-slice-at "-" "abc-def") ;; => '("abc" "-def")
;(s-slice-at "[.#]" "abc.def.ghi#id") ;; => '("abc" ".def" ".ghi" "#id")
;
(define s-split (lambda (separator s : optional False) (let (res (.split s separator)) (if-else optional (list (filter s-present? res) ) res ))))
;Split s into substrings bounded by matches for regexp separator. If omit-nulls is non-nil, zero-length substrings are omitted.
;
;This is a simple wrapper around the built-in split-string.
;
;(s-split "|" "a|bc|12|3") ;; => '("a" "bc" "12" "3")
;(s-split ":" "a,c,d") ;; => '("a,c,d")
;(s-split "\n" "z\nefg\n") ;; => '("z" "efg" "")
;
(define s-split-up-to (lambda (separator s n &optional omit-nulls) ))
;
;Split s up to n times into substrings bounded by matches for regexp separator.
;
;If omit-nulls is non-nil, zero-length substrings are omitted.
;
;See also s-split.
;
;(s-split-up-to "\\s-*-\\s-*" "Author - Track-number-one" 1) ;; => '("Author" "Track-number-one")
;(s-split-up-to "\\s-*-\\s-*" "Author - Track-number-one" 2) ;; => '("Author" "Track" "number-one")
;(s-split-up-to "|" "foo||bar|baz|qux" 3 t) ;; => '("foo" "bar" "baz|qux")
;
(define s-join (lambda (separator strings) .#"separator.join(strings)"))
;
;Join all the strings in strings with separator in between.
;
;(s-join "+" '("abc" "def" "ghi")) ;; => "abc+def+ghi"
;(s-join "\n" '("abc" "def" "ghi")) ;; => "abc\ndef\nghi"
;
(define s-equals? operator..eq)
;
;Is s1 equal to s2?
;
;This is a simple wrapper around the built-in string-equal.
;
;(s-equals? "abc" "ABC") ;; => nil
;(s-equals? "abc" "abc") ;; => t
;
(define s-less? operator..lt)
;
;Is s1 less than s2?
;
;This is a simple wrapper around the built-in string-lessp.
;
;(s-less? "abc" "abd") ;; => t
;(s-less? "abd" "abc") ;; => nil
;(s-less? "abc" "abc") ;; => nil
;
(define s-matches? (lambda (regexp s &optional start) ))
;
;Does regexp match s? If start is non-nil the search starts at that index.
;
;This is a simple wrapper around the built-in string-match-p.
;
;(s-matches? "^[0-9]+$" "123") ;; => t
;(s-matches? "^[0-9]+$" "a123") ;; => nil
;(s-matches? "1" "1a" 1) ;; => nil
;
(define s-blank? (lambda (s) (|| (s-equals? s "") (s-equals? s '())) ))
;
;Is s nil or the empty string?
;
;(s-blank? "") ;; => t
;(s-blank? nil) ;; => t
;(s-blank? " ") ;; => nil
;
(define s-present? (lambda (s) (not (s-blank? s)) ))
;
;Is s anything but nil or the empty string?
;
;(s-present? "") ;; => nil
;(s-present? nil) ;; => nil
;(s-present? " ") ;; => t
;
(define s-ends-with? (lambda (suffix s : ignore-case False) (if-else ignore-case (.endswith  (.lower suffix) (.lower suffix)) (.endswith s suffix) )))
;
;Does s end with suffix?
;
;If ignore-case is non-nil, the comparison is done without paying attention to case differences.
;
(define s-suffix? s-ends-with?)
;
;(s-ends-with? ".md" "readme.md") ;; => t
;(s-ends-with? ".MD" "readme.md") ;; => nil
;(s-ends-with? ".MD" "readme.md" True) ;; => t
;
(define s-starts-with? (lambda (prefix s : ignore-case False) (if-else ignore-case (.startswith  (.lower prefix) (.lower prefix)) (.startswith s prefix) )))
;
;Does s start with prefix?
;
;If ignore-case is non-nil, the comparison is done without paying attention to case differences.
;
(define s-prefix? s-starts-with?); This is a simple wrapper around the built-in string-prefix-p.
;
;(s-starts-with? "lib/" "lib/file.js") ;; => t
;(s-starts-with? "LIB/" "lib/file.js") ;; => nil
;(s-starts-with? "LIB/" "lib/file.js" True) ;; => t
;
(define s-contains? (lambda (needle s : ignore-case False) (if-else ignore-case (.__contains__ (.lower s) (.lower needle)) (.__contains__ s needle)) ))
;
;Does s contain needle?
;
;If ignore-case is non-nil, the comparison is done without paying attention to case differences.
;
;(s-contains? "file" "lib/file.js") ;; => t
;(s-contains? "nope" "lib/file.js") ;; => nil
;(s-contains? "^a" "it's not ^a regexp") ;; => t
;
(define s-lowercase? (lambda (s) (.islower s)))
;
;Are all the letters in s in lower case?
;
;(s-lowercase? "file") ;; => t
;(s-lowercase? "File") ;; => nil
;(s-lowercase? "filä") ;; => t
;
(define s-uppercase? (lambda (s) (.isupper s)))
;
;Are all the letters in s in upper case?
;
;(s-uppercase? "HULK SMASH") ;; => t
;(s-uppercase? "Bruce no smash") ;; => nil
;(s-uppercase? "FöB") ;; => nil
;
(define s-mixedcase? (lambda (s) (not (or (s-lowercase? s) (s-uppercase? s))) ))
;
;Are there both lower case and upper case letters in s?
;
;(s-mixedcase? "HULK SMASH") ;; => nil
;(s-mixedcase? "Bruce no smash") ;; => t
;(s-mixedcase? "BRÜCE") ;; => nil
;
(define s-capitalized? (lambda (s) (s-equals? s (s-capitalize s))))
;
;In s, is the first letter upper case, and all other letters lower case?
;
;(s-capitalized? "Capitalized") ;; => t
;(s-capitalized? "I am capitalized") ;; => t
;(s-capitalized? "I Am Titleized") ;; => nil
;
(define s-numeric? (lambda (s) .#"s.isnumeric()"))
;
;Is s a number?
;
;(s-numeric? "123") ;; => t
;(s-numeric? "onetwothree") ;; => nil
;(s-numeric? "7a") ;; => nil
;
(define s-replace (lambda (old new s) .#"s.replace(old,new)"))
;
;Replaces old with new in s.
;
;(s-replace "file" "nope" "lib/file.js") ;; => "lib/nope.js"
;(s-replace "^a" "\\1" "it's not ^a regexp") ;; => "it's not \\1 regexp"
;
(define s-replace-all (lambda (replacements s) ))
;
;replacements is a list of cons-cells. Each car is replaced with cdr in s.
;
;(s-replace-all '(("lib" . "test") ("file" . "file_test")) "lib/file.js") ;; => "test/file_test.js"
;(s-replace-all '(("lib" . "test") ("test" . "lib")) "lib/test.js") ;; => "test/lib.js"
;
(define s-downcase (lambda (s) (.lower s)))
;
;Convert s to lower case.
;
;This is a simple wrapper around the built-in downcase.
;
;(s-downcase "ABC") ;; => "abc"
;
(define s-upcase (lambda (s) (.upper s)))
;
;Convert s to upper case.
;
;This is a simple wrapper around the built-in upcase.
;
;(s-upcase "abc") ;; => "ABC"
;
(define s-capitalize (lambda (s) (.capitalize s)))
;
;Convert the first word's first character to upper case and the rest to lower case in s.
;
;(s-capitalize "abc DEF") ;; => "Abc def"
;(s-capitalize "abc.DEF") ;; => "Abc.def"
;
(define s-titleize (lambda (s) (.title s)))
;
;Convert each word's first character to upper case and the rest to lower case in s.
;
;This is a simple wrapper around the built-in capitalize.
;
;(s-titleize "abc DEF") ;; => "Abc Def"
;(s-titleize "abc.DEF") ;; => "Abc.Def"
;
;(define s-with (lambda (s form &rest more) ))
;(setattr _macro_ 's-with '->>)
(defmacro s-with (expr : :* forms)
  "``->>`` 'Thread-last'.

  Converts a pipeline to function calls by recursively threading
  expressions as the last argument of the next form.
  E.g. ``(->> x (A b) (C d e))`` is ``(C d e (A b x))``.
  Can replace partial application in some cases.
  Also works inside a ``->`` pipeline.
  E.g. ``(-> x (A a) (->> B b) (C c))`` is ``(C (B b (A x a)) c)``.
  "
  (if-else forms
    `(->> (,@(operator..getitem forms 0) ,expr)
          ,@(operator..getitem forms (slice 1 None)))
    expr))
;
;Threads s through the forms. Inserts s as the last item in the first form, making a list of it if it is not a list already. If there are more forms, inserts the first form as the last item in second form, etc.
;
;(s-with "   hulk smash   " s-trim s-upcase) ;; => "HULK SMASH"
;(s-with "My car is a Toyota" (s-replace "car" "name") (s-replace "a Toyota" "Bond") (s-append ", James Bond")) ;; => "My name is Bond, James Bond"
;(s-with "abc \ndef  \nghi" s-lines (mapcar 's-trim) (s-join "-") s-reverse) ;; => "ihg-fed-cba"
;
(define s-index-of (lambda (needle s : ignore-case False)                     (let (indx (if-else ignore-case   (.find  (.lower s)  (.lower needle)) (.find s needle))) (if-else (operator..eq -1 indx) False indx))))
;
;Returns first index of needle in s, or nil.
;
;If ignore-case is non-nil, the comparison is done without paying attention to case differences.
;
;(s-index-of "abc" "abcdef") ;; => 0
;(s-index-of "CDE" "abcdef" True) ;; => 2
;(s-index-of "n.t" "not a regexp") ;; => nil
;
(define s-reverse (lambda (s) .#"s[::-1]" ))
;
;Return the reverse of s.
;
;(s-reverse "abc") ;; => "cba"
;(s-reverse "ab xyz") ;; => "zyx ba"
;(s-reverse "") ;; => ""
;
(define s-presence (lambda (s) (if-else s s False)))
;
;Return s if it's s-present?, otherwise return nil.
;
;(s-presence nil) ;; => nil
;(s-presence "") ;; => nil
;(s-presence "foo") ;; => "foo"
;
(define s-format (lambda (template replacer &optional extra) ))
;
;Format template with the function replacer.
;
;replacer takes an argument of the format variable and optionally an extra argument which is the extra value from the call to s-format.
;
;Several standard s-format helper functions are recognized and adapted for this:
;
;(s-format "${name}" 'gethash hash-table)
;(s-format "${name}" 'aget alist)
;(s-format "$0" 'elt sequence)
;
;The replacer function may be used to do any other kind of transformation.
;
;(s-format "help ${name}! I'm ${malady}" 'aget '(("name" . "nic") ("malady" . "on fire"))) ;; => "help nic! I'm on fire"
;(s-format "hello ${name}, nice day" (lambda (var-name) "nic")) ;; => "hello nic, nice day"
;(s-format "hello $0, nice $1" 'elt '("nic" "day")) ;; => "hello nic, nice day"
;
(define s-lex-format (lambda (format-str) ))
;
;s-format with the current environment.
;
;format-str may use the s-format variable reference to refer to any variable:
;
;(let ((x 1)) (s-lex-format "x is: ${x}"))
;
;The values of the variables are interpolated with "%s" unless the variable s-lex-value-as-lisp is t and then they are interpolated with "%S".
;
;(let ((x 1)) (s-lex-format "x is ${x}")) ;; => "x is 1"
;(let ((str1 "this") (str2 "that")) (s-lex-format "${str1} and ${str2}")) ;; => "this and that"
;(let ((foo "Hello\\nWorld")) (s-lex-format "${foo}")) ;; => "Hello\\nWorld"
;
(define s-count-matches (lambda (regexp s &optional start end) ))
;
;Count occurrences of regexp in `s'.
;
;start, inclusive, and end, exclusive, delimit the part of s to match.
;
;(s-count-matches "a" "aba") ;; => 2
;(s-count-matches "a" "aba" 0 2) ;; => 1
;(s-count-matches "\\w\\{2\\}[0-9]+" "ab1bab2frobinator") ;; => 2
;
(define s-wrap (lambda (s prefix  : suffix False ) (if-else suffix (s-concat prefix s suffix) (s-concat prefix s prefix)) ))
;
;Wrap string s with prefix and optionally suffix.
;
;Return string s with prefix prepended. If suffix is present, it is appended, otherwise prefix is used as both prefix and suffix.
;
;(s-wrap "foo" "\"") ;; => "\"foo\""
;(s-wrap "foo" "(" ")") ;; => "(foo)"
;(s-wrap "foo" "bar") ;; => "barfoobar"
;
.#"import re"
.#"RE_WORDS = re.compile(r'''[A-Z]+(?=[A-Z][a-z]) | [A-Z]?[a-z]+ | [A-Z]+ |  \d+ ''', re.VERBOSE)" ;thank you dear stackoverflow user for helping me out with this brainfuck0.5-lite exercise!!! Now I have even more time to read about caravaggio or watch some movies from Almodovar
(define s-split-words (lambda (s) (->> s (s-replace "-" " ") (s-replace "_" " ") (RE_WORDS.findall))))
;
;Split s into list of words.
;
;(s-split-words "under_score") ;; => '("under" "score")
;(s-split-words "some-dashed-words") ;; => '("some" "dashed" "words")
;(s-split-words "evenCamelCase") ;; => '("even" "Camel" "Case")
;
(define s-lower-camel-case (lambda (s) (let (words (-map s-downcase (s-split-words s)))  (reduce + (+ .#"[words[0]]" (-map s-capitalize .#"words[1:]" )  )))))
;
;Convert s to lowerCamelCase.
;
;(s-lower-camel-case "some words") ;; => "someWords"
;(s-lower-camel-case "dashed-words") ;; => "dashedWords"
;(s-lower-camel-case "under_scored_words") ;; => "underScoredWords"
;
(define s-upper-camel-case (lambda (s) (let (words (-map s-downcase (s-split-words s)))  (reduce +  (-map s-capitalize words )  ))))
;
;Convert s to UpperCamelCase.
;
;(s-upper-camel-case "some words") ;; => "SomeWords"
;(s-upper-camel-case "dashed-words") ;; => "DashedWords"
;(s-upper-camel-case "under_scored_words") ;; => "UnderScoredWords"
;
(define s-snake-case (lambda (s) (let (words (-map s-downcase (s-split-words s)))  (s-join "_" words )  )))
;
;Convert s to snake_case.
;
;(s-snake-case "some words") ;; => "some_words"
;(s-snake-case "dashed-words") ;; => "dashed_words"
;(s-snake-case "camelCasedWords") ;; => "camel_cased_words"
;
(define s-dashed-words (lambda (s) (let (words (-map s-downcase (s-split-words s)))  (s-join "-" words )  )))
;
;Convert s to dashed-words.
;
;(s-dashed-words "some words") ;; => "some-words"
;(s-dashed-words "under_scored_words") ;; => "under-scored-words"
;(s-dashed-words "camelCasedWords") ;; => "camel-cased-words"
;
(define s-capitalized-words (lambda (s) (let (words (-map s-downcase (s-split-words s)))  (s-capitalize (s-join " " words )  ))))
;
;Convert s to Capitalized words.
;
;(s-capitalized-words "some words") ;; => "Some words"
;(s-capitalized-words "under_scored_words") ;; => "Under scored words"
;(s-capitalized-words "camelCasedWords") ;; => "Camel cased words"
;
(define s-titleized-words (lambda (s) (let (words (-map s-capitalize (s-split-words s)))   (s-join " " words )  )))
;
;Convert s to Titleized Words.
;
;(s-titleized-words "some words") ;; => "Some Words"
;(s-titleized-words "under_scored_words") ;; => "Under Scored Words"
;(s-titleized-words "camelCasedWords") ;; => "Camel Cased Words"
;
(define s-word-initials (lambda (s) (reduce + (-map first (s-split-words s))) ))
;
;Convert s to its initials.
;
;(s-word-initials "some words") ;; => "sw"
;(s-word-initials "under_scored_words") ;; => "usw"
;(s-word-initials "camelCasedWords") ;; => "cCW"
;
;
;Long live Krypton!
